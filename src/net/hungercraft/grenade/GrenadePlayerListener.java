package net.hungercraft.grenade;


import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;

public class GrenadePlayerListener implements Listener {
	
	HashMap<String, Long> playerCooldowns;
	
	public GrenadePlayerListener()
	{
		this.playerCooldowns = new HashMap<String, Long>();
	}
	
	@EventHandler
	public void onPlayerGrenade( PlayerTeleportEvent event )
	{
		if( event.getCause().equals(TeleportCause.ENDER_PEARL) )
		{		
			World world = event.getTo().getWorld();
			
			//Create fake explosion
			world.createExplosion( event.getTo(), 0.0f );
			
			List<Entity> entities = world.getEntities();
			
	        for(Entity e : entities)
	        {
	        	//Check if entity is alive
	            if( !(e instanceof LivingEntity) )
	                    continue;
	            
	            if( e instanceof Player )
	            {
	            	if( e != event.getPlayer() )
	            		continue;
	            }
	            
	            //Caclulate distance between entity and location
	            Location entityLoc = e.getLocation();
	            double distance = entityLoc.distance( event.getTo() );
	            
	            //If in range, damage them
	            if(distance <= 4)
	            {
	                ((LivingEntity)e).damage(8);
	            }
	 
	        }
			
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onGrenadeThrow( ProjectileLaunchEvent event )
	{
		if( event.getEntity() instanceof EnderPearl )
		{
			EnderPearl pearl = (EnderPearl)event.getEntity();
			
			if( pearl.getShooter() instanceof Player )
			{
				Player player = (Player)pearl.getShooter();
				
				Long time = this.playerCooldowns.get( player.getName() );
				Long currentTime = System.currentTimeMillis();
				
				if( time != null && time + 5000 > currentTime )
				{
					double seconds = ((time + 5000) - currentTime) / 1000;
					
					DecimalFormat df = new DecimalFormat("#.##");
					
					player.sendMessage( ChatColor.RED + "You cannot throw a grenade for " + df.format(seconds) + " second(s)!");
					player.getInventory().addItem(new ItemStack(Material.ENDER_PEARL, 1));
					event.setCancelled( true );
				}
				else
				{
					player.sendMessage( ChatColor.RED + "Grenade out!");
					this.playerCooldowns.put( player.getName(), System.currentTimeMillis() );
				}
			}
			
			
		}
	}

}
