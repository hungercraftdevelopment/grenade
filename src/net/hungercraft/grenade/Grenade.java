package net.hungercraft.grenade;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Grenade extends JavaPlugin {
	
	
	public void onEnable()
	{
		getLogger().info("Grenades have been enabled!");
		PluginManager pm = this.getServer().getPluginManager();
		
		pm.registerEvents(new GrenadePlayerListener(), this);
	}
	
	public void onDisable()
	{
		getLogger().info("Grenades have been enabled!");
	}

}
